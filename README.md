# Liste de choses à faire

Projet de fin de cours d'AJD.
Mon projet a pour but de créer une TodoList en utilisant AngularJS et Mongodb. 

## Build
Il faut au préalable installer Angular CLI 
npm install -g @angular/cli

### Appli
Après avoir fait un npm install il faut lancer l'application avec la commande ng serve

### Base de donnée
Il faut au préalable installer deployd sur la machine 
La base de donnée utilisée est Mongodb
Se rendre dans le dossier todos et lancer la commande dpd pour lancer la BDD
Elle peut être monitoré en utilisant localhost:2403/dashboard

