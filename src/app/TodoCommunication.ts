import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import {Todo} from './Todo';
@Injectable()
export class TodoCommunication{

    private newTodo = new Subject<string>();
    private updateTodo = new Subject<Todo>();

    newTodoStream = this.newTodo.asObservable();
    updateTodoStream = this.updateTodo.asObservable();
    
    constructor(){

    }

    newTodoCom(){
        this.newTodo.next("ok");
    }

    updateTodoCom(todo){
        this.updateTodo.next(todo);

    }



}